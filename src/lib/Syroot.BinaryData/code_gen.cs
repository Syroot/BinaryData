﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    // ---- Decimal ----
    
    public static partial class StreamExtensions
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/decimal/read/*'/>
        public static T Read<T>(this T stream, out Decimal result) where T : Stream
            => ReadDecimal(stream, out result);
        
        /// <include file='doc_gen.xml' path='doc/decimal/read/*'/>
        public static Decimal ReadDecimal(this Stream stream)
        {
            ReadDecimal(stream, out Decimal result);
            return result;
        }
        
        /// <include file='doc_gen.xml' path='doc/decimal/read/*'/>
        public static T ReadDecimal<T>(this T stream, out Decimal result) where T : Stream
        {
            FillBuffer(stream, sizeof(Decimal));
            result = ByteConverter.ToDecimal(Buffer);
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/decimal/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public static async Task<Decimal> ReadDecimalAsync(this Stream stream, CancellationToken ct = default)
            => ByteConverter.ToDecimal(await FillBufferAsync(stream, sizeof(Decimal), ct));

        // --- Write ---
        
        /// <include file='doc_gen.xml' path='doc/decimal/write/*'/>
        public static T Write<T>(this T stream, Decimal value) where T : Stream
            => WriteDecimal(stream, value);

        /// <include file='doc_gen.xml' path='doc/decimal/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public static async Task WriteAsync(this Stream stream, Decimal value, CancellationToken ct = default)
            => await WriteDecimalAsync(stream, value, ct);

        /// <include file='doc_gen.xml' path='doc/decimal/write/*'/>
        public static T WriteDecimal<T>(this T stream, Decimal value) where T : Stream
        {
            byte[] buffer = Buffer;
            ByteConverter.GetBytes(value, buffer);
            stream.Write(buffer, 0, sizeof(Decimal));
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/decimal/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public static async Task WriteDecimalAsync(this Stream stream, Decimal value, CancellationToken ct = default)
        {
            byte[] buffer = new byte[sizeof(Decimal)];
            ByteConverter.GetBytes(value, buffer);
            await stream.WriteAsync(buffer, 0, sizeof(Decimal), ct);
        }
    }

    public partial class BinaryStream
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/decimal/read/*'/>
        public BinaryStream Read(out Decimal result)
        {
            BaseStream.ReadDecimal(out result);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/decimal/read/*'/>
        public Decimal ReadDecimal()
            => BaseStream.ReadDecimal();

        /// <include file='doc_gen.xml' path='doc/decimal/read/*'/>
        public BinaryStream ReadDecimal(out Decimal result)
        {
            BaseStream.ReadDecimal(out result);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/decimal/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
    public async Task<Decimal> ReadDecimalAsync(CancellationToken ct = default)
            => await BaseStream.ReadDecimalAsync(ct);

        // ---- Write ----
        
        /// <include file='doc_gen.xml' path='doc/decimal/write/*'/>
        public BinaryStream Write(Decimal value)
        {
            BaseStream.Write(value);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/decimal/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(Decimal value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ct);

        /// <include file='doc_gen.xml' path='doc/decimal/write/*'/>
        public BinaryStream WriteDecimal(Decimal value)
        {
            BaseStream.Write(value);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/decimal/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteDecimalAsync(Decimal value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ct);
    }

    // ---- Double ----
    
    public static partial class StreamExtensions
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/double/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Read<T>(this T stream, out Double result, ByteConverter converter = null) where T : Stream
            => ReadDouble(stream, out result, converter);
        
        /// <include file='doc_gen.xml' path='doc/double/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static Double ReadDouble(this Stream stream, ByteConverter converter = null)
        {
            ReadDouble(stream, out Double result, converter);
            return result;
        }
        
        /// <include file='doc_gen.xml' path='doc/double/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T ReadDouble<T>(this T stream, out Double result, ByteConverter converter = null) where T : Stream
        {
            FillBuffer(stream, sizeof(Double));
            result = (converter ?? ByteConverter.System).ToDouble(Buffer);
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/double/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task<Double> ReadDoubleAsync(this Stream stream, ByteConverter converter = null, CancellationToken ct = default)
            => (converter ?? ByteConverter.System).ToDouble(await FillBufferAsync(stream, sizeof(Double), ct));

        // --- Write ---
        
        /// <include file='doc_gen.xml' path='doc/double/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Write<T>(this T stream, Double value, ByteConverter converter = null) where T : Stream
            => WriteDouble(stream, value, converter);

        /// <include file='doc_gen.xml' path='doc/double/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteAsync(this Stream stream, Double value, ByteConverter converter = null, CancellationToken ct = default)
            => await WriteDoubleAsync(stream, value, converter, ct);

        /// <include file='doc_gen.xml' path='doc/double/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T WriteDouble<T>(this T stream, Double value, ByteConverter converter = null) where T : Stream
        {
            byte[] buffer = Buffer;
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            stream.Write(buffer, 0, sizeof(Double));
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/double/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteDoubleAsync(this Stream stream, Double value, ByteConverter converter = null, CancellationToken ct = default)
        {
            byte[] buffer = new byte[sizeof(Double)];
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            await stream.WriteAsync(buffer, 0, sizeof(Double), ct);
        }
    }

    public partial class BinaryStream
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/double/read/*'/>
        public BinaryStream Read(out Double result)
        {
            BaseStream.ReadDouble(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/double/read/*'/>
        public Double ReadDouble()
            => BaseStream.ReadDouble(ByteConverter);

        /// <include file='doc_gen.xml' path='doc/double/read/*'/>
        public BinaryStream ReadDouble(out Double result)
        {
            BaseStream.ReadDouble(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/double/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<Double> ReadDoubleAsync(CancellationToken ct = default)
            => await BaseStream.ReadDoubleAsync(ByteConverter, ct);

        // ---- Write ----
        
        /// <include file='doc_gen.xml' path='doc/double/write/*'/>
        public BinaryStream Write(Double value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/double/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(Double value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/double/write/*'/>
        public BinaryStream WriteDouble(Double value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/double/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteDoubleAsync(Double value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);
    }

    // ---- Int16 ----
    
    public static partial class StreamExtensions
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/int16/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Read<T>(this T stream, out Int16 result, ByteConverter converter = null) where T : Stream
            => ReadInt16(stream, out result, converter);
        
        /// <include file='doc_gen.xml' path='doc/int16/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static Int16 ReadInt16(this Stream stream, ByteConverter converter = null)
        {
            ReadInt16(stream, out Int16 result, converter);
            return result;
        }
        
        /// <include file='doc_gen.xml' path='doc/int16/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T ReadInt16<T>(this T stream, out Int16 result, ByteConverter converter = null) where T : Stream
        {
            FillBuffer(stream, sizeof(Int16));
            result = (converter ?? ByteConverter.System).ToInt16(Buffer);
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/int16/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task<Int16> ReadInt16Async(this Stream stream, ByteConverter converter = null, CancellationToken ct = default)
            => (converter ?? ByteConverter.System).ToInt16(await FillBufferAsync(stream, sizeof(Int16), ct));

        // --- Write ---
        
        /// <include file='doc_gen.xml' path='doc/int16/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Write<T>(this T stream, Int16 value, ByteConverter converter = null) where T : Stream
            => WriteInt16(stream, value, converter);

        /// <include file='doc_gen.xml' path='doc/int16/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteAsync(this Stream stream, Int16 value, ByteConverter converter = null, CancellationToken ct = default)
            => await WriteInt16Async(stream, value, converter, ct);

        /// <include file='doc_gen.xml' path='doc/int16/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T WriteInt16<T>(this T stream, Int16 value, ByteConverter converter = null) where T : Stream
        {
            byte[] buffer = Buffer;
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            stream.Write(buffer, 0, sizeof(Int16));
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/int16/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteInt16Async(this Stream stream, Int16 value, ByteConverter converter = null, CancellationToken ct = default)
        {
            byte[] buffer = new byte[sizeof(Int16)];
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            await stream.WriteAsync(buffer, 0, sizeof(Int16), ct);
        }
    }

    public partial class BinaryStream
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/int16/read/*'/>
        public BinaryStream Read(out Int16 result)
        {
            BaseStream.ReadInt16(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int16/read/*'/>
        public Int16 ReadInt16()
            => BaseStream.ReadInt16(ByteConverter);

        /// <include file='doc_gen.xml' path='doc/int16/read/*'/>
        public BinaryStream ReadInt16(out Int16 result)
        {
            BaseStream.ReadInt16(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int16/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<Int16> ReadInt16Async(CancellationToken ct = default)
            => await BaseStream.ReadInt16Async(ByteConverter, ct);

        // ---- Write ----
        
        /// <include file='doc_gen.xml' path='doc/int16/write/*'/>
        public BinaryStream Write(Int16 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int16/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(Int16 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/int16/write/*'/>
        public BinaryStream WriteInt16(Int16 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int16/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteInt16Async(Int16 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);
    }

    // ---- Int32 ----
    
    public static partial class StreamExtensions
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/int32/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Read<T>(this T stream, out Int32 result, ByteConverter converter = null) where T : Stream
            => ReadInt32(stream, out result, converter);
        
        /// <include file='doc_gen.xml' path='doc/int32/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static Int32 ReadInt32(this Stream stream, ByteConverter converter = null)
        {
            ReadInt32(stream, out Int32 result, converter);
            return result;
        }
        
        /// <include file='doc_gen.xml' path='doc/int32/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T ReadInt32<T>(this T stream, out Int32 result, ByteConverter converter = null) where T : Stream
        {
            FillBuffer(stream, sizeof(Int32));
            result = (converter ?? ByteConverter.System).ToInt32(Buffer);
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/int32/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task<Int32> ReadInt32Async(this Stream stream, ByteConverter converter = null, CancellationToken ct = default)
            => (converter ?? ByteConverter.System).ToInt32(await FillBufferAsync(stream, sizeof(Int32), ct));

        // --- Write ---
        
        /// <include file='doc_gen.xml' path='doc/int32/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Write<T>(this T stream, Int32 value, ByteConverter converter = null) where T : Stream
            => WriteInt32(stream, value, converter);

        /// <include file='doc_gen.xml' path='doc/int32/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteAsync(this Stream stream, Int32 value, ByteConverter converter = null, CancellationToken ct = default)
            => await WriteInt32Async(stream, value, converter, ct);

        /// <include file='doc_gen.xml' path='doc/int32/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T WriteInt32<T>(this T stream, Int32 value, ByteConverter converter = null) where T : Stream
        {
            byte[] buffer = Buffer;
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            stream.Write(buffer, 0, sizeof(Int32));
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/int32/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteInt32Async(this Stream stream, Int32 value, ByteConverter converter = null, CancellationToken ct = default)
        {
            byte[] buffer = new byte[sizeof(Int32)];
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            await stream.WriteAsync(buffer, 0, sizeof(Int32), ct);
        }
    }

    public partial class BinaryStream
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/int32/read/*'/>
        public BinaryStream Read(out Int32 result)
        {
            BaseStream.ReadInt32(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int32/read/*'/>
        public Int32 ReadInt32()
            => BaseStream.ReadInt32(ByteConverter);

        /// <include file='doc_gen.xml' path='doc/int32/read/*'/>
        public BinaryStream ReadInt32(out Int32 result)
        {
            BaseStream.ReadInt32(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int32/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<Int32> ReadInt32Async(CancellationToken ct = default)
            => await BaseStream.ReadInt32Async(ByteConverter, ct);

        // ---- Write ----
        
        /// <include file='doc_gen.xml' path='doc/int32/write/*'/>
        public BinaryStream Write(Int32 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int32/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(Int32 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/int32/write/*'/>
        public BinaryStream WriteInt32(Int32 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int32/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteInt32Async(Int32 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);
    }

    // ---- Int64 ----
    
    public static partial class StreamExtensions
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/int64/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Read<T>(this T stream, out Int64 result, ByteConverter converter = null) where T : Stream
            => ReadInt64(stream, out result, converter);
        
        /// <include file='doc_gen.xml' path='doc/int64/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static Int64 ReadInt64(this Stream stream, ByteConverter converter = null)
        {
            ReadInt64(stream, out Int64 result, converter);
            return result;
        }
        
        /// <include file='doc_gen.xml' path='doc/int64/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T ReadInt64<T>(this T stream, out Int64 result, ByteConverter converter = null) where T : Stream
        {
            FillBuffer(stream, sizeof(Int64));
            result = (converter ?? ByteConverter.System).ToInt64(Buffer);
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/int64/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task<Int64> ReadInt64Async(this Stream stream, ByteConverter converter = null, CancellationToken ct = default)
            => (converter ?? ByteConverter.System).ToInt64(await FillBufferAsync(stream, sizeof(Int64), ct));

        // --- Write ---
        
        /// <include file='doc_gen.xml' path='doc/int64/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Write<T>(this T stream, Int64 value, ByteConverter converter = null) where T : Stream
            => WriteInt64(stream, value, converter);

        /// <include file='doc_gen.xml' path='doc/int64/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteAsync(this Stream stream, Int64 value, ByteConverter converter = null, CancellationToken ct = default)
            => await WriteInt64Async(stream, value, converter, ct);

        /// <include file='doc_gen.xml' path='doc/int64/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T WriteInt64<T>(this T stream, Int64 value, ByteConverter converter = null) where T : Stream
        {
            byte[] buffer = Buffer;
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            stream.Write(buffer, 0, sizeof(Int64));
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/int64/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteInt64Async(this Stream stream, Int64 value, ByteConverter converter = null, CancellationToken ct = default)
        {
            byte[] buffer = new byte[sizeof(Int64)];
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            await stream.WriteAsync(buffer, 0, sizeof(Int64), ct);
        }
    }

    public partial class BinaryStream
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/int64/read/*'/>
        public BinaryStream Read(out Int64 result)
        {
            BaseStream.ReadInt64(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int64/read/*'/>
        public Int64 ReadInt64()
            => BaseStream.ReadInt64(ByteConverter);

        /// <include file='doc_gen.xml' path='doc/int64/read/*'/>
        public BinaryStream ReadInt64(out Int64 result)
        {
            BaseStream.ReadInt64(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int64/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<Int64> ReadInt64Async(CancellationToken ct = default)
            => await BaseStream.ReadInt64Async(ByteConverter, ct);

        // ---- Write ----
        
        /// <include file='doc_gen.xml' path='doc/int64/write/*'/>
        public BinaryStream Write(Int64 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int64/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(Int64 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/int64/write/*'/>
        public BinaryStream WriteInt64(Int64 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/int64/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteInt64Async(Int64 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);
    }

    // ---- UInt16 ----
    
    public static partial class StreamExtensions
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/uint16/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Read<T>(this T stream, out UInt16 result, ByteConverter converter = null) where T : Stream
            => ReadUInt16(stream, out result, converter);
        
        /// <include file='doc_gen.xml' path='doc/uint16/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static UInt16 ReadUInt16(this Stream stream, ByteConverter converter = null)
        {
            ReadUInt16(stream, out UInt16 result, converter);
            return result;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint16/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T ReadUInt16<T>(this T stream, out UInt16 result, ByteConverter converter = null) where T : Stream
        {
            FillBuffer(stream, sizeof(UInt16));
            result = (converter ?? ByteConverter.System).ToUInt16(Buffer);
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint16/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task<UInt16> ReadUInt16Async(this Stream stream, ByteConverter converter = null, CancellationToken ct = default)
            => (converter ?? ByteConverter.System).ToUInt16(await FillBufferAsync(stream, sizeof(UInt16), ct));

        // --- Write ---
        
        /// <include file='doc_gen.xml' path='doc/uint16/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Write<T>(this T stream, UInt16 value, ByteConverter converter = null) where T : Stream
            => WriteUInt16(stream, value, converter);

        /// <include file='doc_gen.xml' path='doc/uint16/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteAsync(this Stream stream, UInt16 value, ByteConverter converter = null, CancellationToken ct = default)
            => await WriteUInt16Async(stream, value, converter, ct);

        /// <include file='doc_gen.xml' path='doc/uint16/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T WriteUInt16<T>(this T stream, UInt16 value, ByteConverter converter = null) where T : Stream
        {
            byte[] buffer = Buffer;
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            stream.Write(buffer, 0, sizeof(UInt16));
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint16/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteUInt16Async(this Stream stream, UInt16 value, ByteConverter converter = null, CancellationToken ct = default)
        {
            byte[] buffer = new byte[sizeof(UInt16)];
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            await stream.WriteAsync(buffer, 0, sizeof(UInt16), ct);
        }
    }

    public partial class BinaryStream
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/uint16/read/*'/>
        public BinaryStream Read(out UInt16 result)
        {
            BaseStream.ReadUInt16(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint16/read/*'/>
        public UInt16 ReadUInt16()
            => BaseStream.ReadUInt16(ByteConverter);

        /// <include file='doc_gen.xml' path='doc/uint16/read/*'/>
        public BinaryStream ReadUInt16(out UInt16 result)
        {
            BaseStream.ReadUInt16(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint16/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<UInt16> ReadUInt16Async(CancellationToken ct = default)
            => await BaseStream.ReadUInt16Async(ByteConverter, ct);

        // ---- Write ----
        
        /// <include file='doc_gen.xml' path='doc/uint16/write/*'/>
        public BinaryStream Write(UInt16 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint16/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(UInt16 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/uint16/write/*'/>
        public BinaryStream WriteUInt16(UInt16 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint16/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteUInt16Async(UInt16 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);
    }

    // ---- UInt32 ----
    
    public static partial class StreamExtensions
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/uint32/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Read<T>(this T stream, out UInt32 result, ByteConverter converter = null) where T : Stream
            => ReadUInt32(stream, out result, converter);
        
        /// <include file='doc_gen.xml' path='doc/uint32/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static UInt32 ReadUInt32(this Stream stream, ByteConverter converter = null)
        {
            ReadUInt32(stream, out UInt32 result, converter);
            return result;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint32/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T ReadUInt32<T>(this T stream, out UInt32 result, ByteConverter converter = null) where T : Stream
        {
            FillBuffer(stream, sizeof(UInt32));
            result = (converter ?? ByteConverter.System).ToUInt32(Buffer);
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint32/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task<UInt32> ReadUInt32Async(this Stream stream, ByteConverter converter = null, CancellationToken ct = default)
            => (converter ?? ByteConverter.System).ToUInt32(await FillBufferAsync(stream, sizeof(UInt32), ct));

        // --- Write ---
        
        /// <include file='doc_gen.xml' path='doc/uint32/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Write<T>(this T stream, UInt32 value, ByteConverter converter = null) where T : Stream
            => WriteUInt32(stream, value, converter);

        /// <include file='doc_gen.xml' path='doc/uint32/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteAsync(this Stream stream, UInt32 value, ByteConverter converter = null, CancellationToken ct = default)
            => await WriteUInt32Async(stream, value, converter, ct);

        /// <include file='doc_gen.xml' path='doc/uint32/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T WriteUInt32<T>(this T stream, UInt32 value, ByteConverter converter = null) where T : Stream
        {
            byte[] buffer = Buffer;
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            stream.Write(buffer, 0, sizeof(UInt32));
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint32/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteUInt32Async(this Stream stream, UInt32 value, ByteConverter converter = null, CancellationToken ct = default)
        {
            byte[] buffer = new byte[sizeof(UInt32)];
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            await stream.WriteAsync(buffer, 0, sizeof(UInt32), ct);
        }
    }

    public partial class BinaryStream
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/uint32/read/*'/>
        public BinaryStream Read(out UInt32 result)
        {
            BaseStream.ReadUInt32(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint32/read/*'/>
        public UInt32 ReadUInt32()
            => BaseStream.ReadUInt32(ByteConverter);

        /// <include file='doc_gen.xml' path='doc/uint32/read/*'/>
        public BinaryStream ReadUInt32(out UInt32 result)
        {
            BaseStream.ReadUInt32(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint32/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<UInt32> ReadUInt32Async(CancellationToken ct = default)
            => await BaseStream.ReadUInt32Async(ByteConverter, ct);

        // ---- Write ----
        
        /// <include file='doc_gen.xml' path='doc/uint32/write/*'/>
        public BinaryStream Write(UInt32 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint32/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(UInt32 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/uint32/write/*'/>
        public BinaryStream WriteUInt32(UInt32 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint32/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteUInt32Async(UInt32 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);
    }

    // ---- UInt64 ----
    
    public static partial class StreamExtensions
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/uint64/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Read<T>(this T stream, out UInt64 result, ByteConverter converter = null) where T : Stream
            => ReadUInt64(stream, out result, converter);
        
        /// <include file='doc_gen.xml' path='doc/uint64/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static UInt64 ReadUInt64(this Stream stream, ByteConverter converter = null)
        {
            ReadUInt64(stream, out UInt64 result, converter);
            return result;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint64/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T ReadUInt64<T>(this T stream, out UInt64 result, ByteConverter converter = null) where T : Stream
        {
            FillBuffer(stream, sizeof(UInt64));
            result = (converter ?? ByteConverter.System).ToUInt64(Buffer);
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint64/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task<UInt64> ReadUInt64Async(this Stream stream, ByteConverter converter = null, CancellationToken ct = default)
            => (converter ?? ByteConverter.System).ToUInt64(await FillBufferAsync(stream, sizeof(UInt64), ct));

        // --- Write ---
        
        /// <include file='doc_gen.xml' path='doc/uint64/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Write<T>(this T stream, UInt64 value, ByteConverter converter = null) where T : Stream
            => WriteUInt64(stream, value, converter);

        /// <include file='doc_gen.xml' path='doc/uint64/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteAsync(this Stream stream, UInt64 value, ByteConverter converter = null, CancellationToken ct = default)
            => await WriteUInt64Async(stream, value, converter, ct);

        /// <include file='doc_gen.xml' path='doc/uint64/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T WriteUInt64<T>(this T stream, UInt64 value, ByteConverter converter = null) where T : Stream
        {
            byte[] buffer = Buffer;
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            stream.Write(buffer, 0, sizeof(UInt64));
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint64/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteUInt64Async(this Stream stream, UInt64 value, ByteConverter converter = null, CancellationToken ct = default)
        {
            byte[] buffer = new byte[sizeof(UInt64)];
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            await stream.WriteAsync(buffer, 0, sizeof(UInt64), ct);
        }
    }

    public partial class BinaryStream
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/uint64/read/*'/>
        public BinaryStream Read(out UInt64 result)
        {
            BaseStream.ReadUInt64(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint64/read/*'/>
        public UInt64 ReadUInt64()
            => BaseStream.ReadUInt64(ByteConverter);

        /// <include file='doc_gen.xml' path='doc/uint64/read/*'/>
        public BinaryStream ReadUInt64(out UInt64 result)
        {
            BaseStream.ReadUInt64(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint64/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<UInt64> ReadUInt64Async(CancellationToken ct = default)
            => await BaseStream.ReadUInt64Async(ByteConverter, ct);

        // ---- Write ----
        
        /// <include file='doc_gen.xml' path='doc/uint64/write/*'/>
        public BinaryStream Write(UInt64 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint64/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(UInt64 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/uint64/write/*'/>
        public BinaryStream WriteUInt64(UInt64 value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/uint64/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteUInt64Async(UInt64 value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);
    }

    // ---- Single ----
    
    public static partial class StreamExtensions
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/single/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Read<T>(this T stream, out Single result, ByteConverter converter = null) where T : Stream
            => ReadSingle(stream, out result, converter);
        
        /// <include file='doc_gen.xml' path='doc/single/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static Single ReadSingle(this Stream stream, ByteConverter converter = null)
        {
            ReadSingle(stream, out Single result, converter);
            return result;
        }
        
        /// <include file='doc_gen.xml' path='doc/single/read/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T ReadSingle<T>(this T stream, out Single result, ByteConverter converter = null) where T : Stream
        {
            FillBuffer(stream, sizeof(Single));
            result = (converter ?? ByteConverter.System).ToSingle(Buffer);
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/single/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task<Single> ReadSingleAsync(this Stream stream, ByteConverter converter = null, CancellationToken ct = default)
            => (converter ?? ByteConverter.System).ToSingle(await FillBufferAsync(stream, sizeof(Single), ct));

        // --- Write ---
        
        /// <include file='doc_gen.xml' path='doc/single/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T Write<T>(this T stream, Single value, ByteConverter converter = null) where T : Stream
            => WriteSingle(stream, value, converter);

        /// <include file='doc_gen.xml' path='doc/single/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteAsync(this Stream stream, Single value, ByteConverter converter = null, CancellationToken ct = default)
            => await WriteSingleAsync(stream, value, converter, ct);

        /// <include file='doc_gen.xml' path='doc/single/write/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static T WriteSingle<T>(this T stream, Single value, ByteConverter converter = null) where T : Stream
        {
            byte[] buffer = Buffer;
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            stream.Write(buffer, 0, sizeof(Single));
            return stream;
        }
        
        /// <include file='doc_gen.xml' path='doc/single/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <include file='doc.xml' path='doc/any/converter/*'/>
        public static async Task WriteSingleAsync(this Stream stream, Single value, ByteConverter converter = null, CancellationToken ct = default)
        {
            byte[] buffer = new byte[sizeof(Single)];
            (converter ?? ByteConverter.System).GetBytes(value, buffer);
            await stream.WriteAsync(buffer, 0, sizeof(Single), ct);
        }
    }

    public partial class BinaryStream
    {
        // ---- Read ----
        
        /// <include file='doc_gen.xml' path='doc/single/read/*'/>
        public BinaryStream Read(out Single result)
        {
            BaseStream.ReadSingle(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/single/read/*'/>
        public Single ReadSingle()
            => BaseStream.ReadSingle(ByteConverter);

        /// <include file='doc_gen.xml' path='doc/single/read/*'/>
        public BinaryStream ReadSingle(out Single result)
        {
            BaseStream.ReadSingle(out result, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/single/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<Single> ReadSingleAsync(CancellationToken ct = default)
            => await BaseStream.ReadSingleAsync(ByteConverter, ct);

        // ---- Write ----
        
        /// <include file='doc_gen.xml' path='doc/single/write/*'/>
        public BinaryStream Write(Single value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/single/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(Single value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/single/write/*'/>
        public BinaryStream WriteSingle(Single value)
        {
            BaseStream.Write(value, ByteConverter);
            return this;
        }
        
        /// <include file='doc_gen.xml' path='doc/single/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteSingleAsync(Single value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ByteConverter, ct);
    }

}
