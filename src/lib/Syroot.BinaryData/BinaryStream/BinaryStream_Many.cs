﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- Iterators ----

        /// <summary>
        /// Invokes the <paramref name="action"/> for every index in the <paramref name="enumerable"/>.
        /// </summary>
        /// <param name="enumerable">The <see cref="IEnumerable"/> to iterate over.</param>
        /// <param name="action">The callback invoked for every element, receiving the current index.</param>
        /// <returns>The current <see cref="BinaryStream"/> instance.</returns>
        public BinaryStream For(IEnumerable enumerable, Action<int> action)
        {
            BaseStream.For(enumerable, action);
            return this;
        }

        /// <summary>
        /// Invokes the <paramref name="action"/> for every element in the <paramref name="enumerable"/>.
        /// </summary>
        /// <typeparam name="TItem">The type of the elements.</typeparam>
        /// <param name="enumerable">The <see cref="IEnumerable"/> to iterate over.</param>
        /// <param name="action">The callback invoked for every element, receiving the current element.</param>
        /// <returns>The current <see cref="BinaryStream"/> instance.</returns>
        public BinaryStream ForEach<TItem>(IEnumerable<TItem> enumerable, Action<TItem> action)
        {
            BaseStream.ForEach(enumerable, action);
            return this;
        }

        // ---- Read ----

        /// <summary>
        /// Returns <paramref name="count"/> instances of type <typeparamref name="TItem"/> read from the underlying
        /// stream by calling the <paramref name="readCallback"/> until all elements are read.
        /// </summary>
        /// <typeparam name="TItem">The type of the instances to read.</typeparam>
        /// <param name="count">The number of instances to read.</param>
        /// <param name="readCallback">A callback returning the element read.</param>
        /// <returns>The read instances.</returns>
        public TItem[] Read<TItem>(int count, Func<TItem> readCallback)
            => BaseStream.ReadMany(count, readCallback);

        /// <summary>
        /// Returns <paramref name="count"/> instances of type <typeparamref name="TItem"/> read asynchronously from the
        /// underlying stream by calling the <paramref name="readCallback"/> until all elements are read.
        /// </summary>
        /// <typeparam name="TItem">The type of the instances to read.</typeparam>
        /// <param name="count">The number of instances to read.</param>
        /// <param name="readCallback">A callback returning the element read.</param>
        /// <returns>The read instances.</returns>
        public async Task<TItem[]> ReadAsync<TItem>(int count, Func<Task<TItem>> readCallback)
            => await BaseStream.ReadManyAsync(count, readCallback);

        /// <summary>
        /// Returns <paramref name="count"/> instances of type <typeparamref name="TItem"/> continually read from the
        /// underlying stream by calling the <paramref name="readCallback"/>.
        /// </summary>
        /// <typeparam name="TItem">The type of the instances to read.</typeparam>
        /// <param name="count">The number of instances to read.</param>
        /// <param name="readCallback">A callback returning the element read.</param>
        /// <returns>The read instances.</returns>
        public TItem[] ReadMany<TItem>(int count, Func<TItem> readCallback)
            => BaseStream.ReadMany(count, readCallback);

        /// <summary>
        /// Returns <paramref name="count"/> instances of type <typeparamref name="TItem"/> read asynchronously from the
        /// underlying stream by calling the <paramref name="readCallback"/> until all elements are read.
        /// </summary>
        /// <typeparam name="TItem">The type of the instances to read.</typeparam>
        /// <param name="count">The number of instances to read.</param>
        /// <param name="readCallback">A callback returning the element read.</param>
        /// <returns>The read instances.</returns>
        public async Task<TItem[]> ReadManyAsync<TItem>(int count, Func<Task<TItem>> readCallback)
            => await BaseStream.ReadManyAsync(count, readCallback);

        // ---- Write ----

        /// <summary>
        /// Writes the <paramref name="values"/> to the underlying stream through the <paramref name="writeCallback"/>
        /// invoked for each value.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="writeCallback">A callback receiving the element to write.</param>
        /// <returns>The current <see cref="BinaryStream"/> instance.</returns>
        public BinaryStream Write<TItem>(IEnumerable<TItem> values, Action<TItem> writeCallback)
        {
            BaseStream.WriteMany(values, writeCallback);
            return this;
        }

        /// <summary>
        /// Writes the <paramref name="values"/> to the underlying stream asynchronously through the
        /// <paramref name="writeCallback"/> invoked for each value.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="writeCallback">A callback receiving the element to write.</param>
        public async Task WriteAsync<TItem>(IEnumerable<TItem> values, Func<TItem, Task> writeCallback)
            => await BaseStream.WriteManyAsync(values, writeCallback);

        /// <summary>
        /// Writes the <paramref name="values"/> to the underlying stream through the <paramref name="writeCallback"/>
        /// invoked for each value.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="writeCallback">A callback receiving the element to write.</param>
        /// <returns>The current <see cref="BinaryStream"/> instance.</returns>
        public BinaryStream WriteMany<TItem>(IEnumerable<TItem> values, Action<TItem> writeCallback)
        {
            BaseStream.WriteMany(values, writeCallback);
            return this;
        }

        /// <summary>
        /// Writes the <paramref name="values"/> to the underlying stream asynchronously through the
        /// <paramref name="writeCallback"/> invoked for each value.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="writeCallback">A callback receiving the element to write.</param>
        public async Task WriteManyAsync<TItem>(IEnumerable<TItem> values, Func<TItem, Task> writeCallback)
            => await BaseStream.WriteManyAsync(values, writeCallback);
    }
}
