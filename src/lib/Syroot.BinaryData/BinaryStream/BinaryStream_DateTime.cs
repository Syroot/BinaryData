﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // --- Read ----

        /// <include file='doc_gen.xml' path='doc/datetime/read/*'/>
        public BinaryStream Read(out DateTime result)
        {
            BaseStream.ReadDateTime(out result, DateTimeCoding, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/datetime/read/*'/>
        public DateTime ReadDateTime()
            => BaseStream.ReadDateTime(DateTimeCoding, ByteConverter);

        /// <include file='doc_gen.xml' path='doc/datetime/read/*'/>
        public BinaryStream ReadDateTime(out DateTime result)
        {
            BaseStream.ReadDateTime(out result, DateTimeCoding, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/datetime/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<DateTime> ReadDateTimeAsync(CancellationToken ct = default)
            => await BaseStream.ReadDateTimeAsync(DateTimeCoding, ByteConverter, ct);

        // ---- Write ----

        /// <include file='doc_gen.xml' path='doc/datetime/write/*'/>
        public BinaryStream Write(DateTime value)
        {
            BaseStream.Write(value, DateTimeCoding, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/datetime/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(DateTime value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, DateTimeCoding, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/datetime/write/*'/>
        public BinaryStream WriteDateTime(DateTime value)
        {
            BaseStream.Write(value, DateTimeCoding, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/datetime/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteDateTimeAsync(DateTime value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, DateTimeCoding, ByteConverter, ct);
    }
}
