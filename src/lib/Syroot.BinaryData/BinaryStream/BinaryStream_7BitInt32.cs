﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- Read ----

        /// <include file='doc_gen.xml' path='doc/_7bitin32/read/*'/>
        public Int32 Read7BitInt32()
            => BaseStream.Read7BitInt32();

        /// <include file='doc_gen.xml' path='doc/_7bitin32/read/*'/>
        public BinaryStream Read7BitInt32(out Int32 result)
        {
            BaseStream.Read7BitInt32(out result);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/_7bitin32/read/*'/>
        public async Task<Int32> Read7BitInt32Async(CancellationToken ct = default)
            => await BaseStream.Read7BitInt32Async(ct);

        // ---- Write ----

        /// <include file='doc_gen.xml' path='doc/_7bitin32/write/*'/>
        public BinaryStream Write7BitInt32(Int32 value)
        {
            BaseStream.Write7BitInt32(value);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/_7bitin32/write/*'/>
        public async Task Write7BitInt32Async(Int32 value, CancellationToken ct = default)
            => await BaseStream.Write7BitInt32Async(value, ct);
    }
}
