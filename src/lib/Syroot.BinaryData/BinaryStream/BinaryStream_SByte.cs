﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- Read ----

        /// <include file='doc_gen.xml' path='doc/sbyte/read/*'/>
        public BinaryStream Read(out SByte result)
        {
            BaseStream.ReadSByte(out result);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/sbyte/read/*'/>
        public SByte ReadSByte()
            => BaseStream.ReadSByte();

        /// <include file='doc.xml' path='doc/sbyte/read/*'/>
        public BinaryStream ReadSByte(out SByte result)
        {
            BaseStream.ReadSByte(out result);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/sbyte/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<SByte> ReadSByteAsync(CancellationToken ct = default)
            => await BaseStream.ReadSByteAsync(ct);

        // ---- Write ----

        /// <include file='doc_gen.xml' path='doc/sbyte/write/*'/>
        public BinaryStream Write(SByte value)
        {
            BaseStream.Write(value);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/sbyte/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(SByte value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ct);

        /// <include file='doc.xml' path='doc/sbyte/write/*'/>
        public BinaryStream WriteSByte(SByte value)
        {
            BaseStream.Write(value);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/sbyte/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteSByteAsync(SByte value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ct);
    }
}
