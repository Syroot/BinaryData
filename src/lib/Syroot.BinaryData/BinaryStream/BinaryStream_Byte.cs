﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- Read ----

        /// <include file='doc_gen.xml' path='doc/byte/read/*'/>
        public BinaryStream Read(out Byte result)
        {
            BaseStream.ReadByte(out result);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/byte/read/*'/>
        /// <remarks>Special method name as it otherwise conflicts with <see cref="Stream.ReadByte"/>.</remarks>
        public Byte Read1Byte()
            => BaseStream.Read1Byte();

        /// <include file='doc_gen.xml' path='doc/byte/read/*'/>
        public BinaryStream ReadByte(out Byte result)
        {
            BaseStream.ReadByte(out result);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/byte/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<byte> ReadByteAsync(CancellationToken ct = default)
            => await BaseStream.ReadByteAsync(ct);

        // ---- Write ----

        /// <include file='doc_gen.xml' path='doc/byte/write/*'/>
        public BinaryStream Write(Byte value)
        {
            BaseStream.WriteByte(value); // use System.IO.Stream implementation directly.
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/byte/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(Byte value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ct);

        /// <include file='doc_gen.xml' path='doc/byte/write/*'/>
        /// <remarks>Special method name as it otherwise conflicts with <see cref="Stream.WriteByte(Byte)"/>.</remarks>
        public BinaryStream Write1Byte(Byte value)
        {
            BaseStream.WriteByte(value); // use System.IO.Stream implementation directly.
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/byte/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteByteAsync(Byte value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, ct);
    }
}
