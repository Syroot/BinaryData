﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- Read Type ----

        /// <include file='doc_gen.xml' path='doc/enum/read/*'/>
        /// <include file='doc.xml' path='doc/enum/type/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        public object ReadEnum(Type type, bool strict = false)
            => BaseStream.ReadEnum(type, strict, ByteConverter);

        /// <include file='doc_gen.xml' path='doc/enum/read/*'/>
        /// <include file='doc.xml' path='doc/enum/type/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        public BinaryStream ReadEnum(out object result, Type type, bool strict = false)
        {
            BaseStream.ReadEnum(out result, type, strict, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/enum/read/*'/>
        /// <include file='doc.xml' path='doc/enum/type/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<object> ReadEnumAsync(Type type, bool strict = false, CancellationToken ct = default)
            => await BaseStream.ReadEnumAsync(type, strict, ByteConverter, ct);

        // ---- Read Generic ----

        /// <include file='doc_gen.xml' path='doc/enum/read/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        public TEnum Read<TEnum>(bool strict = false) where TEnum : Enum
            => (TEnum)BaseStream.ReadEnum(typeof(TEnum), strict, ByteConverter);

        /// <include file='doc_gen.xml' path='doc/enum/read/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        public TEnum ReadEnum<TEnum>(bool strict = false) where TEnum : Enum
            => (TEnum)BaseStream.ReadEnum(typeof(TEnum), strict, ByteConverter);

        /// <include file='doc_gen.xml' path='doc/enum/read/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        public BinaryStream ReadEnum<TEnum>(out TEnum result, bool strict = false) where TEnum : Enum
        {
            BaseStream.ReadEnum(out result, strict, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/enum/read/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<T> ReadEnumAsync<T>(bool strict = false, CancellationToken ct = default) where T : Enum
            => (T)await BaseStream.ReadEnumAsync(typeof(T), strict, ByteConverter, ct);

        // ---- Write Type ----

        /// <include file='doc_gen.xml' path='doc/enum/write/*'/>
        /// <include file='doc.xml' path='doc/enum/type/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        public BinaryStream WriteEnum(Type type, object value, bool strict = false)
        {
            BaseStream.WriteEnum(type, value, strict, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/enum/write/*'/>
        /// <include file='doc.xml' path='doc/enum/type/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteEnumAsync(Type type, object value, bool strict = false, CancellationToken ct = default)
            => await BaseStream.WriteEnumAsync(type, value, strict, ByteConverter, ct);

        // ---- Write Generic ----

        /// <include file='doc_gen.xml' path='doc/enum/write/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        public BinaryStream Write<T>(T value, bool strict = false) where T : Enum
        {
            BaseStream.WriteEnum(value, strict, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/enum/write/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync<T>(T value, bool strict = false, CancellationToken ct = default) where T : Enum
            => await BaseStream.WriteEnumAsync(value, strict, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/enum/write/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        public BinaryStream WriteEnum<T>(T value, bool strict = false) where T : Enum
        {
            BaseStream.WriteEnum(value, strict, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/enum/write/*'/>
        /// <include file='doc.xml' path='doc/enum/strict/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteEnumAsync<T>(T value, bool strict = false, CancellationToken ct = default) where T : Enum
            => await BaseStream.WriteEnumAsync(value, strict, ByteConverter, ct);
    }
}
