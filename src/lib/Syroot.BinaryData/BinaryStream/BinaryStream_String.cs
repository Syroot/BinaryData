﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- Read with StringCoding ----

        /// <include file='doc_gen.xml' path='doc/string/read/*'/>
        public BinaryStream Read(out String result)
        {
            BaseStream.ReadString(out result, StringCoding, Encoding, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/string/read/*'/>
        public String ReadString()
            => BaseStream.ReadString(StringCoding, Encoding, ByteConverter);

        /// <include file='doc_gen.xml' path='doc/string/read/*'/>
        public BinaryStream ReadString(out String result)
        {
            BaseStream.ReadString(out result, StringCoding, Encoding, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/string/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<String> ReadStringAsync(CancellationToken ct = default)
            => await BaseStream.ReadStringAsync(StringCoding, Encoding, ByteConverter, ct);

        // ---- Read with length ----

        /// <include file='doc_gen.xml' path='doc/string/read/*'/>
        /// <include file='doc.xml' path='doc/string/length/*'/>
        public BinaryStream Read(out String result, int length)
        {
            BaseStream.ReadString(out result, length);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/string/read/*'/>
        /// <include file='doc.xml' path='doc/string/length/*'/>
        public String ReadString(int length)
            => BaseStream.ReadString(length, Encoding);

        /// <include file='doc_gen.xml' path='doc/string/read/*'/>
        /// <include file='doc.xml' path='doc/string/length/*'/>
        public BinaryStream ReadString(out String result, int length)
        {
            BaseStream.ReadString(out result, length, Encoding);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/string/read/*'/>
        /// <include file='doc.xml' path='doc/string/length/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<String> ReadStringAsync(int length, CancellationToken ct = default)
            => await BaseStream.ReadStringAsync(length, Encoding, ct);

        // ---- Write ----

        /// <include file='doc_gen.xml' path='doc/string/write/*'/>
        public BinaryStream Write(String value)
        {
            BaseStream.Write(value, StringCoding, Encoding, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/string/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(String value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, StringCoding, Encoding, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/string/write/*'/>
        public BinaryStream WriteString(String value)
        {
            BaseStream.Write(value, StringCoding, Encoding, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/string/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteStringAsync(String value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, StringCoding, Encoding, ByteConverter, ct);
    }
}
