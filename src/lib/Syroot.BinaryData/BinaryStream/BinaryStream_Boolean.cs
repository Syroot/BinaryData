﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- Read ----

        /// <include file='doc_gen.xml' path='doc/boolean/read/*'/>
        public BinaryStream Read(out Boolean result)
        {
            BaseStream.ReadBoolean(out result, BooleanCoding);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/boolean/read/*'/>
        public Boolean ReadBoolean()
            => BaseStream.ReadBoolean(BooleanCoding);

        /// <include file='doc_gen.xml' path='doc/boolean/read/*'/>
        public BinaryStream ReadBoolean(out Boolean result)
        {
            BaseStream.ReadBoolean(out result, BooleanCoding);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/boolean/read/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task<Boolean> ReadBooleanAsync(CancellationToken ct = default)
            => await BaseStream.ReadBooleanAsync(BooleanCoding, ct);

        // ---- Write ----

        /// <include file='doc_gen.xml' path='doc/boolean/write/*'/>
        public BinaryStream Write(Boolean value)
        {
            BaseStream.Write(value, BooleanCoding, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/boolean/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteAsync(Boolean value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, BooleanCoding, ByteConverter, ct);

        /// <include file='doc_gen.xml' path='doc/boolean/write/*'/>>
        public BinaryStream WriteBoolean(Boolean value)
        {
            BaseStream.Write(value, BooleanCoding, ByteConverter);
            return this;
        }

        /// <include file='doc_gen.xml' path='doc/boolean/write/*'/>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        public async Task WriteBooleanAsync(Boolean value, CancellationToken ct = default)
            => await BaseStream.WriteAsync(value, BooleanCoding, ByteConverter, ct);
    }
}
