﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- Read ----

        /// <summary>
        /// Reads <see cref="Byte"/> values into <paramref name="result"/> in one call.
        /// </summary>
        /// <param name="result">The read values.</param>
        /// <returns>The current <see cref="BinaryStream"/> instance.</returns>
        public BinaryStream ReadBytes(byte[] result)
        {
            BaseStream.ReadBytes(result);
            return this;
        }

        /// <summary>
        /// Reads <see cref="Byte"/> values into <paramref name="result"/> in one call.
        /// </summary>
        /// <param name="result">The read values.</param>
        /// <returns>The current <see cref="BinaryStream"/> instance.</returns>
        public BinaryStream ReadBytes(ArraySegment<byte> result)
        {
            BaseStream.ReadBytes(result);
            return this;
        }

#if !NETSTANDARD2_0
        /// <summary>
        /// Reads <see cref="Byte"/> values into <paramref name="result"/> in one call.
        /// </summary>
        /// <param name="result">The read values.</param>
        /// <returns>The current <see cref="BinaryStream"/> instance.</returns>
        public BinaryStream ReadBytes(Span<byte> result)
        {
            BaseStream.ReadBytes(result);
            return this;
        }
#endif
        // ---- Read Async ----

        /// <summary>
        /// Returns <see cref="Byte"/> values read asynchronously from the stream in one call.
        /// </summary>
        /// <param name="count">The number of values to read.</param>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <returns>The read values.</returns>
        public async Task<byte[]> ReadBytesAsync(int count, CancellationToken ct = default)
            => await BaseStream.ReadBytesAsync(count, ct);

        // ---- Write ----

        /// <summary>
        /// Writes <see cref="Byte"/> values to the stream in one call.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <returns>The current <see cref="BinaryStream"/> instance.</returns>
        public BinaryStream WriteBytes(byte[] values)
        {
            BaseStream.WriteBytes(values);
            return this;
        }

        /// <summary>
        /// Writes <see cref="Byte"/> values to the stream in one call.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <returns>The current <see cref="BinaryStream"/> instance.</returns>
        public BinaryStream WriteBytes(ArraySegment<byte> values)
        {
            BaseStream.WriteBytes(values);
            return this;
        }

#if !NETSTANDARD2_0
        /// <summary>
        /// Writes <see cref="Byte"/> values to the stream in one call.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <returns>The current <see cref="BinaryStream"/> instance.</returns>
        public BinaryStream WriteBytes(ReadOnlySpan<byte> values)
        {
            BaseStream.WriteBytes(values);
            return this;
        }
#endif
        // ---- Write Async ----

#pragma warning disable IDE0067 // Dispose objects before losing scope. BaseStream is not created in the called method.
        /// <summary>
        /// Writes <see cref="Byte"/> values asynchronously to the stream in one call.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <returns>The current stream instance.</returns>
        public async Task<BinaryStream> WriteBytesAsync(byte[] values, CancellationToken ct = default)
        {
            await BaseStream.WriteBytesAsync(values, ct);
            return this;
        }

        /// <summary>
        /// Writes <see cref="Byte"/> values asynchronously to the stream in one call.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <returns>The current stream instance.</returns>
        public async Task<BinaryStream> WriteBytesAsync(ArraySegment<byte> values, CancellationToken ct = default)
        {
            await BaseStream.WriteBytesAsync(values, ct);
            return this;
        }

#if !NETSTANDARD2_0
        /// <summary>
        /// Writes <see cref="Byte"/> values asynchronously to the stream in one call.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <include file='doc.xml' path='doc/any/ct/*'/>
        /// <returns>The current stream instance.</returns>
        public async Task<BinaryStream> WriteBytesAsync(ReadOnlyMemory<byte> values, CancellationToken ct = default)
        {
            await BaseStream.WriteBytesAsync(values, ct);
            return this;
        }
#endif
#pragma warning restore IDE0067
    }
}
