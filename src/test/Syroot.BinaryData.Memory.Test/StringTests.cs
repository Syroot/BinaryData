using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Syroot.BinaryData.Memory.Test
{
    [TestClass]
    public class StringTests
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly List<(Encoding encoding, string text)> _tests = new List<(Encoding, string)>
        {
            (Encoding.ASCII, "Many love Donald Drumpf. I heard that many people also love a blender up their asshole."),
            (null, "Hello, World! «ταБЬℓσ»"),
            (Encoding.UTF8, "٩(-̮̮̃-̃)۶ ٩(●̮̮̃•̃)۶ ٩(͡๏̯͡๏)۶ ٩(-̮̮̃•̃)."),
            (Encoding.UTF8, "Testing «ταБЬℓσ»: 1<2 & 4+1>3, now 20% off!"),
            (Encoding.Unicode, "Smiley balls 👨👩👧👧"),
            (Encoding.UTF32, "Smiley balls 👨👩👧👧"),
            (Encoding.BigEndianUnicode, "Smiley balls 👨👩👧👧"),
            (Encodings.ShiftJIS, "Hey you, 日本へようこそ！"),
            (Encodings.ShiftJIS, "日本へようこそ！"),
            (Encodings.UHC, "Not Korean"),
            (Encodings.UHC, "여보세요 World!"),
            (Encodings.UHC, "Say 여보세요"),
            (Encodings.UHC, "Say 여보세요, someone!"),
            (Encoding.UTF8, "Ź̢͕͚̖̬͖͚̫͙̣̫͘À͓̮̬̙̦͙̗̦̗̰̰̗̯̹̥̰̙̞̮L̀҉̧̠̲̯͚͔͈̟͇͈͘͠ͅG͜҉̝͕̻̩̼͓̝̦̹̫͉͔͠O̢̥̦͔̰͓̩͇̲͇͔͍̕͘!̴͔̮̰̯"),
            (Encoding.UTF8, "M᷿᷿͢͏̴᷿᷿᷿᷿͢͢͏᷿᷿͢e᷿᷿͢͏̴᷿᷿᷿᷿͢͢͏᷿᷿͢s᷿᷿͢͏̴᷿᷿᷿᷿͢͢͏᷿᷿͢s᷿᷿͢͏̴᷿᷿᷿᷿͢͢͏᷿᷿͢")
        };

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        [TestMethod]
        public void String()
        {
            Span<byte> buffer = stackalloc byte[2048];

            SpanWriter writer = new SpanWriter(buffer);
            foreach (var (encoding, text) in _tests)
            {
                writer.Encoding = encoding;
                writer.WriteString(text);
            }

            SpanReader reader = new SpanReader(buffer);
            foreach (var (encoding, text) in _tests)
            {
                reader.Encoding = encoding;
                Assert.AreEqual(text, reader.ReadString());
            }
        }

        [TestMethod]
        public void String0()
        {
            Span<byte> buffer = stackalloc byte[2048];

            SpanWriter writer = new SpanWriter(buffer);
            foreach (var (encoding, text) in _tests)
            {
                writer.Encoding = encoding;
                writer.WriteString0(text);
            }

            SpanReader reader = new SpanReader(buffer);
            foreach (var (encoding, text) in _tests)
            {
                reader.Encoding = encoding;
                Assert.AreEqual(text, reader.ReadString0());
            }
        }

        [TestMethod]
        public void String1()
        {
            Span<byte> buffer = stackalloc byte[2048];

            SpanWriter writer = new SpanWriter(buffer);
            foreach (var (encoding, text) in _tests)
            {
                writer.Encoding = encoding;
                writer.WriteString1(text);
            }

            SpanReader reader = new SpanReader(buffer);
            foreach (var (encoding, text) in _tests)
            {
                reader.Encoding = encoding;
                Assert.AreEqual(text, reader.ReadString1());
            }
        }

        [TestMethod]
        public void String2()
        {
            Span<byte> buffer = stackalloc byte[2048];

            SpanWriter writer = new SpanWriter(buffer);
            foreach (var (encoding, text) in _tests)
            {
                writer.Encoding = encoding;
                writer.WriteString2(text);
            }

            SpanReader reader = new SpanReader(buffer);
            foreach (var (encoding, text) in _tests)
            {
                reader.Encoding = encoding;
                Assert.AreEqual(text, reader.ReadString2());
            }
        }

        [TestMethod]
        public void String4()
        {
            Span<byte> buffer = stackalloc byte[2048];

            SpanWriter writer = new SpanWriter(buffer);
            foreach (var (encoding, text) in _tests)
            {
                writer.Encoding = encoding;
                writer.WriteString4(text);
            }

            SpanReader reader = new SpanReader(buffer);
            foreach (var (encoding, text) in _tests)
            {
                reader.Encoding = encoding;
                Assert.AreEqual(text, reader.ReadString4());
            }
        }

        [TestMethod]
        public void StringFix()
        {
            int byteCount = 1024;
            Span<byte> buffer = stackalloc byte[byteCount * _tests.Count];
            buffer.Fill(0xFF); // Check zeroing out memory.

            SpanWriter writer = new SpanWriter(buffer);
            foreach (var (encoding, text) in _tests)
            {
                writer.Encoding = encoding;
                writer.WriteStringFix(text, byteCount);
            }

            SpanReader reader = new SpanReader(buffer);
            foreach (var (encoding, text) in _tests)
            {
                reader.Encoding = encoding;
                Assert.AreEqual(text, reader.ReadStringFix(byteCount));
            }
        }
    }
}
