# BinaryData

When parsing or storing data of binary file formats or protocols, the default .NET functionality is often insufficient. Classes like `BinaryReader` or `BinaryWriter` lack support for a different byte order (endianness) than the system one. Strings are typically stored with a length-prefix other than common 0 terminating bytes. Navigation inside `Stream` instances may be uncomfortable.

The `Syroot.BinaryData` libraries fill these gaps by providing the following features:
- Different byte order (endianness), supporting big and little endian the same way.
- Many `String` formats like 0-termianted strings or UInt16 length prefixes.
- Several `Boolean` and `DateTime` formats (like 4-byte booleans or CTime's `time_t` integer).
- Strongly typed support for saving enum values and checking them for validity.
- Performant and simple parsing of data available in `(ReadOnly)Span<byte>` instances.
- Advanced `Stream` navigation features like temporary seeks, simulated seeks, or (negative) alignments.
- Serializing and deserializing any class into a binary format.

## Documentation

A feature tour aswell as complete API documentation is available on [Syroot Docs](https://docs.syroot.com/binarydata).
